from setuptools import setup

package_name = 'idl_transform_pkg'

setup(
    name=package_name,
    version='1.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='void',
    maintainer_email='marius-egeland@hotmail.com',
    description='Transform Odom msg to TFs',
    license='Use and abuse',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
                'ros_node_tf_odom = idl_transform_pkg.transformPublisher:main',
                'ros_node_groundTruth_publisher = idl_transform_pkg.gazeboGroundTruthPublisher:main',
                'ros_node_pc2_restamper = idl_transform_pkg.pointCloudRestamper:main',
                'ros_node_zedm_restamper = idl_transform_pkg.pointCloudRestamperZedMini:main',
        ],
    },
)
