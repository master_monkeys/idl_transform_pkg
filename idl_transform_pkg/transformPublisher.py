

# RCL py stuff
import rclpy
from rclpy.node import Node
from rclpy.qos import qos_profile_sensor_data
from rclpy.time import Time

# Transform 2 specifics
import tf2_ros
from tf2_ros import TransformBroadcaster, StaticTransformBroadcaster

# Ros2 msgs
from geometry_msgs.msg import TransformStamped
from nav_msgs.msg import Odometry
from std_msgs.msg import Header
from sensor_msgs.msg import PointCloud2

# Maths
import numpy as np
from scipy.spatial.transform import Rotation as R

# Self made stuff
from idl_botsy_pkg.droneConfiguration import DroneGeometry
from idl_botsy_pkg.softwareConfigutarion import simulation


class Odom_tf_Subscriber(Node):

    def __init__(self):
        super().__init__('odom_tf_subscriber')

        print('Odom_tf_Subscriber Created')

        ## Time stuff
        self.__starTime = self.get_clock().now().to_msg()

        # Creating instance of geometric constants
        droneGeom = DroneGeometry()

        ## Static transforms

        # Creating static transform broadcaster and defining static broadcasts
        self.static_transform_broadcaster = StaticTransformBroadcaster(self)
        # Transform vec to be broadcasted
        staticTtVec = []

        # ned to map stf
        # Geting data from droneGeom and parsing
        pos_n_nm = droneGeom.pos_n_nm
        tHeta_mn = droneGeom.tHeta_mn

        ned_map_stf = self.staticTransformGenerator(tHeta_mn, -pos_n_nm, 'map_idl', 'ned_idl')
        staticTtVec.append(ned_map_stf)

        # Map to map in zed mini
        if simulation == False:
            zeroVec = np.zeros((3,1))
            ned_mapMap_stf = self.staticTransformGenerator(zeroVec, zeroVec, 'map_idl', 'map')
            staticTtVec.append(ned_mapMap_stf)

        # Body to main imu
        # Geting data from droneGeom and parsing
        pos_b_bu = droneGeom.pos_b_bu
        tHeta_bu = droneGeom.tHeta_bu
        
        body_imuMain_stf = self.staticTransformGenerator(tHeta_bu, pos_b_bu, 'body_ekf', 'imu_main')
        staticTtVec.append(body_imuMain_stf)

        # Body to aux imu
        # Geting data from droneGeom and parsing
        pos_b_bv = droneGeom.pos_b_bv
        tHeta_bv = droneGeom.tHeta_bv
        
        body_imuAux_stf = self.staticTransformGenerator(tHeta_bv, pos_b_bv, 'body_ekf', 'imu_aux')
        staticTtVec.append(body_imuAux_stf)

        # Body to camera
        # Geting data from droneGeom and parsing
        pos_b_bc = droneGeom.pos_b_bc
        tHeta_bc = droneGeom.tHeta_bc
        
        body_camera_stf = self.staticTransformGenerator(tHeta_bc, pos_b_bc, 'body_ekf', 'camera')
        staticTtVec.append(body_camera_stf)

        # Broadcasting static transforms
        self.static_transform_broadcaster.sendTransform(staticTtVec)

        ## Creating Dynamic transform broadcaste
        self.transform_broadcaster = TransformBroadcaster(self)

        ### Publishing one ned to body msg in init
        fakeNedBodyOdomMsg = Odometry()
        fakeNedBodyOdomMsg.header.stamp = self.get_clock().now().to_msg()
        fakeNedBodyOdomMsg.header.frame_id = 'ned_idl'
        fakeNedBodyOdomMsg.child_frame_id = 'body_ekf'
        fakeNedBodyOdomMsg.pose.pose.position.x = 0.0
        fakeNedBodyOdomMsg.pose.pose.position.y = 0.0
        fakeNedBodyOdomMsg.pose.pose.position.z = 0.0
        fakeNedBodyOdomMsg.pose.pose.orientation.x = 0.0
        fakeNedBodyOdomMsg.pose.pose.orientation.y = 0.0
        fakeNedBodyOdomMsg.pose.pose.orientation.z = 0.0
        fakeNedBodyOdomMsg.pose.pose.orientation.w = 1.0
        self.odom_callback_body_ekf(fakeNedBodyOdomMsg)

        # Timing stuff
        self.__timeLastEkfMsg = 0.0
        self.__timeMaxWait = 0.3

        ## Timers
        watchDogDt = 0.1
        self.watchDogTimer = self.create_timer(watchDogDt, self.__watchDogTimerCallback)
        
        ## Subscribers
        if simulation:
            self.subscription_odom_imu_aux_sim = self.create_subscription(Odometry, 'botsy/odom/imu_aux_sim', self.odom_callback_imu_aux_sim, qos_profile_sensor_data)
            self.subscription_odom_imu_aux_sim  # prevent unused variable warning

            self.subscription_odom_imu_main_sim = self.create_subscription(Odometry, 'botsy/odom/imu_main_sim', self.odom_callback_imu_main_sim, qos_profile_sensor_data)
            self.subscription_odom_imu_main_sim  # prevent unused variable warning

            self.subscription_odom_body_sim = self.create_subscription(Odometry, 'botsy/odom/body_sim', self.odom_callback_body_sim, qos_profile_sensor_data)
            self.subscription_odom_body_sim  # prevent unused variable warning

        self.subscription_odom_body_ekf = self.create_subscription(Odometry, 'botsy/odom/body', self.odom_callback_body_ekf, qos_profile_sensor_data)
        self.subscription_odom_body_ekf  # prevent unused variable warning

    # Transform helper functions
    def staticTransformGenerator(self, tHeta, pos, fromName, toName):
        '''
        Function to return static transform

        Input   tHeta   np.shape (3,1)
                pos     np.shape (3,1)
                fromName    string
                toName      string
        '''



        pos = pos.astype(float)
        tHeta = tHeta[:, 0]
        rot = R.from_euler('xyz', tHeta, degrees=False)
        quat = rot.as_quat()

        stf = TransformStamped()
        stf.header.stamp = self.get_clock().now().to_msg()
        stf.header.frame_id = fromName
        stf.child_frame_id = toName
        stf.transform.translation.x = pos[0,0]
        stf.transform.translation.y = pos[1,0]
        stf.transform.translation.z = pos[2,0]
        stf.transform.rotation.x = quat[0]
        stf.transform.rotation.y = quat[1]
        stf.transform.rotation.z = quat[2]
        stf.transform.rotation.w = quat[3]

        return stf

    def dynamicTransformGenerator(self, msg, fromName, toName):
        
        dtf = TransformStamped()

        dtf.header = msg.header
        dtf.header.frame_id = fromName
        dtf.child_frame_id = toName
        dtf.transform.translation.x = msg.pose.pose.position.x
        dtf.transform.translation.y = msg.pose.pose.position.y
        dtf.transform.translation.z = msg.pose.pose.position.z
        dtf.transform.rotation = msg.pose.pose.orientation

        return dtf

    # Subscriber callback functions
    def odom_callback_imu_main_sim(self, msg):
        # Creating and populating tf from odom msg

        dtf = self.dynamicTransformGenerator(msg, 'map_idl', 'imu_main_sim')
        dtf.header.stamp = self.get_clock().now().to_msg()

        # Publishing tf data
        self.transform_broadcaster.sendTransform(dtf)

    def odom_callback_imu_aux_sim(self, msg):
        # Creating and populating tf from odom msg

        dtf = self.dynamicTransformGenerator(msg, 'map_idl', 'imu_aux_sim')
        dtf.header.stamp = self.get_clock().now().to_msg()

        # Publishing tf data
        self.transform_broadcaster.sendTransform(dtf)

    def odom_callback_body_sim(self, msg):
        # Creating and populating tf from odom msg

        dtf = self.dynamicTransformGenerator(msg, 'map_idl', 'body_sim')
        dtf.header.stamp = self.get_clock().now().to_msg()

        # Publishing tf data
        self.transform_broadcaster.sendTransform(dtf)

    def odom_callback_body_ekf(self, msg):
        # Creating and populating tf from odom msg

        # Reseting watch dog timer
        timeMsgFormat = self.get_clock().now().to_msg()
        timeSec = Time.from_msg(timeMsgFormat).nanoseconds*10**(-9)

        self.__timeLastEkfMsg = timeSec

        dtf = self.dynamicTransformGenerator(msg, 'ned_idl', 'body_ekf')
        #dtf.header.stamp = self.get_clock().now().to_msg()

        # Publishing tf data
        self.transform_broadcaster.sendTransform(dtf)

    # Timer callback functions
    def __watchDogTimerCallback(self):

        # Geting time
        timeMsgFormat = self.get_clock().now().to_msg()
        timeSec = Time.from_msg(timeMsgFormat).nanoseconds*10**(-9)

        # if time has expired, the ekf body transfer is fixed to the map frame
        if timeSec - self.__timeLastEkfMsg > self.__timeMaxWait:
            self.odom_callback_body_ekf_fallback()

    def odom_callback_body_ekf_fallback(self):
        # Creating and populating tf from odom msg

        msg = Odometry()
        msg.pose.pose.orientation.z = 0.707106781
        msg.pose.pose.orientation.w = 0.707106781

        dtf = self.dynamicTransformGenerator(msg, 'ned_idl', 'body_ekf')
        dtf.header.stamp = self.get_clock().now().to_msg()

        # Publishing tf data
        self.transform_broadcaster.sendTransform(dtf)




def main(args=None):
    rclpy.init(args=args)

    odom_subscriber = Odom_tf_Subscriber()

    rclpy.spin(odom_subscriber)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    odom_subscriber.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
