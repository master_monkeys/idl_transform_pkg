import rclpy
from rclpy.node import Node
from rclpy.qos import qos_profile_sensor_data
from nav_msgs.msg import Odometry
from geometry_msgs.msg import TwistWithCovarianceStamped, PoseWithCovarianceStamped, Vector3Stamped
from sensor_msgs.msg import Imu
import numpy as np
from scipy.spatial.transform import Rotation as R
from idl_botsy_pkg.droneConfiguration import DroneGeometry, Rot




class GroundTruth_Pub(Node):

    def __init__(self):
        super().__init__('ground_truth_pub')

        print('ground_truth_pub Created')
        
        #### Instance of drone geometry & rotator class ####
        self.droneGeom = DroneGeometry()
        self.Rot = Rot()

        #### Const matrix from map to ned ####
        self.rotMat_nm = self.droneGeom.rotMat_nm
        self.rotMat_mn = self.rotMat_nm.T

        #### To hold message data ####
        self.vel_m_mb       = np.zeros((3,1), dtype = np.float32)
        self.tHetaDot_mb    = np.zeros((3,1), dtype = np.float32)

        self.quat_mb        = np.zeros((1,4), dtype = np.float32)
        self.quat_mb[0,3]   = 1.0

        self.pos_m_mb       = np.zeros((3,1), dtype = np.float32)
        self.gt_angVel      = np.zeros((3,1), dtype = np.float32) # From GT Imu
        self.gt_linAcc      = np.zeros((3,1), dtype = np.float32) # From GT Imu

        #### Variables to hold velocities ####
        self.vel_l_nb       = np.zeros((3,1), dtype = np.float32)
        self.angVel_l_nb    = np.zeros((3,1), dtype = np.float32)
        self.vel_b_nb       = np.zeros((3,1), dtype = np.float32)
        self.angVel_b_nb    = np.zeros((3,1), dtype = np.float32)
        
        #### To hold pos and orientation
        self.tHeta_nb = np.zeros((3,1), dtype = np.float32)
        self.pos_n_nb = np.zeros((3,1), dtype = np.float32)

        #### To hold pos and orientation error between ekf and gt
        self.tHetaError_nb = np.zeros((3,1), dtype = np.float32)
        self.posError_n_nb = np.zeros((3,1), dtype = np.float32)

        #### Covariance matrices ####
        self.velocityCovariance = 0.05*np.eye(6) 
        self.poseCovariance = 0.06*np.eye(6) 



        #### Subscribers ####
        self.odom_subscriber    = self.create_subscription(Odometry, 'botsy/odom/body_sim', self.odom_callback, qos_profile_sensor_data)
        self.imu_subscriber     = self.create_subscription(Imu, 'sensor/imu_GT' , self.imu_callback, qos_profile_sensor_data)
        self.ekf_subscriber     = self.create_subscription(PoseWithCovarianceStamped, 'ekf/pose_ned' , self.ekf_callback, 10)

        #### Publishers ####
        self.accPub_body = self.create_publisher(Vector3Stamped, 'gazeboGT/acc_body', 10)
        self.velPub_body = self.create_publisher(TwistWithCovarianceStamped, 'gazeboGT/vel_body', 10)
        self.velPub_levelBody = self.create_publisher(TwistWithCovarianceStamped, 'gazeboGT/vel_level', 10)
        self.posePub_body = self.create_publisher(PoseWithCovarianceStamped, 'gazeboGT/pose_ned', 10)
        self.gtEkfPosError_body = self.create_publisher(Vector3Stamped, 'gazeboGT/ekf_pos_error', 10)
        self.gtEkftHetaError_body = self.create_publisher(Vector3Stamped, 'gazeboGT/ekf_tHeta_error', 10)

        #### Timers ####
        """
        publisher_rate = 10.0
        publisher_dt = 1/publisher_rate
        self.publisher_timer = self.create_timer(publisher_dt, self.publisher_callback)
        """

    # Publisher callback
    def publisher_callback(self):

        # Perform calculations
        self.calculate_position_orientation()
        self.calculate_linear_velocities()
        self.calculate_angular_velocities()

        # Create messages
        levelBodyVelocityMessage = TwistWithCovarianceStamped()
        bodyVelocityMessage = TwistWithCovarianceStamped()
        bodyPoseMessage = PoseWithCovarianceStamped()
        bodyAccMessage = Vector3Stamped()
        

        # Assign timestamp
        timeNow = self.get_clock().now().to_msg()
        levelBodyVelocityMessage.header.stamp = timeNow
        bodyVelocityMessage.header.stamp = timeNow
        bodyPoseMessage.header.stamp = timeNow
        bodyAccMessage.header.stamp = timeNow
        

        #### Populate Pose ####
        ### Positions ###
        posToMsg_n_nb = self.pos_n_nb.astype(np.float64)
        bodyPoseMessage.pose.pose.position.x = posToMsg_n_nb[0, 0]
        bodyPoseMessage.pose.pose.position.y = posToMsg_n_nb[1, 0]
        bodyPoseMessage.pose.pose.position.z = posToMsg_n_nb[2, 0]
        bodyPoseMessage.pose.covariance = self.poseCovariance.reshape(36).astype(np.float64)

        ### Quaternions ###
        ## Not actually a quaternion, but roll/pitch/yaw ##
        tHetaToMsg_nb = self.tHeta_nb.astype(np.float64)
        bodyPoseMessage.pose.pose.orientation.x = tHetaToMsg_nb[0, 0]
        bodyPoseMessage.pose.pose.orientation.y = tHetaToMsg_nb[1, 0]
        bodyPoseMessage.pose.pose.orientation.z = tHetaToMsg_nb[2, 0]
        bodyPoseMessage.pose.pose.orientation.w = -2.0


        #### Populate velocities ####
        ## Body ##
        # Linear #
        velToMsg_b_nb = self.vel_b_nb.astype(np.float64)
        bodyVelocityMessage.twist.twist.linear.x = velToMsg_b_nb[0, 0]
        bodyVelocityMessage.twist.twist.linear.y = velToMsg_b_nb[1, 0]
        bodyVelocityMessage.twist.twist.linear.z = velToMsg_b_nb[2, 0]

        # Covariance #
        bodyVelocityMessage.twist.covariance = self.velocityCovariance.reshape(36).astype(np.float64)

        # Angular #
        angVelToMsg_b_nb = self.angVel_b_nb.astype(np.float64)
        bodyVelocityMessage.twist.twist.angular.x = angVelToMsg_b_nb[0, 0]
        bodyVelocityMessage.twist.twist.angular.y = angVelToMsg_b_nb[1, 0]
        bodyVelocityMessage.twist.twist.angular.z = angVelToMsg_b_nb[2, 0]

        ## Leveled body ##
        # Linear #
        velToMsg_l_nb = self.vel_l_nb.astype(np.float64)
        levelBodyVelocityMessage.twist.twist.linear.x = velToMsg_l_nb[0, 0]
        levelBodyVelocityMessage.twist.twist.linear.y = velToMsg_l_nb[1, 0]
        levelBodyVelocityMessage.twist.twist.linear.z = velToMsg_l_nb[2, 0]

        # Covariance #
        levelBodyVelocityMessage.twist.covariance = self.velocityCovariance.reshape(36).astype(np.float64)

        # Angular #
        angVelToMsg_l_nb = self.angVel_l_nb.astype(np.float64)
        levelBodyVelocityMessage.twist.twist.angular.x = angVelToMsg_l_nb[0, 0]
        levelBodyVelocityMessage.twist.twist.angular.y = angVelToMsg_l_nb[1, 0]
        levelBodyVelocityMessage.twist.twist.angular.z = angVelToMsg_l_nb[2, 0]

        ### Acceleration ###
        accToMsg_b_nb = self.gt_linAcc.astype(np.float64)
        bodyAccMessage.vector.x = accToMsg_b_nb[0, 0]
        bodyAccMessage.vector.y = accToMsg_b_nb[1, 0]
        bodyAccMessage.vector.z = accToMsg_b_nb[2, 0]


        ### Error msg ###
        posErrorMessage = Vector3Stamped()
        tHetaErrorMessage = Vector3Stamped()

        timeNow = self.get_clock().now().to_msg()
        posErrorMessage.header.stamp = timeNow
        tHetaErrorMessage.header.stamp = timeNow

        # Pose
        posErrorToMsg = self.posError_n_nb.astype(np.float64)
        posErrorMessage.vector.x = posErrorToMsg[0,0]
        posErrorMessage.vector.y = posErrorToMsg[1,0]
        posErrorMessage.vector.z = posErrorToMsg[2,0]

        # Theta
        tHetaErrorToMsg = self.tHetaError_nb.astype(np.float64)
        tHetaErrorMessage.vector.x = tHetaErrorToMsg[0,0]
        tHetaErrorMessage.vector.y = tHetaErrorToMsg[1,0]
        tHetaErrorMessage.vector.z = tHetaErrorToMsg[2,0]



        self.gtEkfPosError_body.publish(posErrorMessage)
        self.gtEkftHetaError_body.publish(tHetaErrorMessage)


        # Publish messages
        self.velPub_levelBody.publish(levelBodyVelocityMessage)
        self.velPub_body.publish(bodyVelocityMessage)
        self.posePub_body.publish(bodyPoseMessage)
        self.accPub_body.publish(bodyAccMessage)
        

    # Subscriber callback
    def imu_callback(self, msg):
        # Get imu data
        gt_angVel_msg = msg.angular_velocity
        self.gt_angVel = np.array([[gt_angVel_msg.x], [gt_angVel_msg.y], [gt_angVel_msg.z]], dtype=np.float32)

        gt_linAcc_msg = msg.linear_acceleration
        self.gt_linAcc = np.array([[gt_linAcc_msg.x], [gt_linAcc_msg.y], [gt_linAcc_msg.z]], dtype=np.float32)

    def odom_callback(self, msg):
        '''
            Take in odom message from gazebo
        '''
        # Read position data
        posMsg_mb = msg.pose.pose.position
        self.pos_m_mb = np.array([  [posMsg_mb.x],
                                    [posMsg_mb.y],
                                    [posMsg_mb.z]], dtype = np.float32)

        # Read orientation quaternion
        quaternionMsg_mb = msg.pose.pose.orientation
        self.quat_mb = np.array([quaternionMsg_mb.x, quaternionMsg_mb.y, quaternionMsg_mb.z, quaternionMsg_mb.w])
        
        # Read velocity data
        linVelMsg_mb = msg.twist.twist.linear
        self.vel_m_mb = np.array([  [linVelMsg_mb.x],
                                    [linVelMsg_mb.y],
                                    [linVelMsg_mb.z]], dtype = np.float32)

        # Read angular velocity
        angVelMsg_mb = msg.twist.twist.angular
        self.tHetaDot_mb = np.array([   [angVelMsg_mb.x],
                                        [angVelMsg_mb.y],
                                        [angVelMsg_mb.z]], dtype = np.float32)

        self.publisher_callback()

    def ekf_callback(self, msg):
        
        # Gets data from msg
        px = msg.pose.pose.position.x
        py = msg.pose.pose.position.y
        pz = msg.pose.pose.position.z
        posEkf_n_nb = np.array([[px],[py],[pz]], dtype = np.float32)
        tx = msg.pose.pose.orientation.x
        ty = msg.pose.pose.orientation.y
        tz = msg.pose.pose.orientation.z
        tHetaEkf_nb = np.array([[tx],[ty],[tz]], dtype = np.float32)

        self.posError_n_nb = self.pos_n_nb - posEkf_n_nb
        self.tHetaError_nb[0:2,0] = self.tHeta_nb[0:2,0] - tHetaEkf_nb[0:2,0]
        self.tHetaError_nb[2,0] = self.__innovationYaw(self.tHeta_nb[2,0], tHetaEkf_nb[2,0])
        


    # Functions
    def calculate_position_orientation(self):
        '''
            Function to Convert the position from the odom message to positions and orientations in the NED frame
        '''

        # Get rotation matrix from map to body
        rotMat_mb = R.from_quat(self.quat_mb).as_matrix()
        
        # Find rotation matrix from ned to body
        rotMat_nb = self.rotMat_nm @ rotMat_mb
        
        # Get euler angles from orientation
        tHeta_nb = R.from_matrix(rotMat_nb).as_euler('ZYX', degrees=False).reshape(3)
        self.tHeta_nb = np.array([tHeta_nb[2], tHeta_nb[1], tHeta_nb[0]]).reshape(3,1)
        self.tHeta_nb[2,0] = self.tHeta_nb[2,0] % (2.0*np.pi)   # Mod yaw 2pi as it comes in range [-pi, pi]

        ### Rotate position vector ###
        # From map to local NED frame
        self.pos_n_nb = self.rotMat_mn @ self.pos_m_mb

    def calculate_angular_velocities(self):
        '''
            Function to find angular velocities in different frames
            Naive approach for now, just rotate the angular rates from the odom message,
            should be replaced with imu data (from simulated perfect imu in gazebo)
        '''

        # Read body rates from perfect IMU message
        self.angVel_b_nb = self.gt_angVel
        
        # Rotate rates into level frame
        rotMat_bl = self.droneGeom.rotFun_bl(self.tHeta_nb)
        self.angVel_l_nb = rotMat_bl @ self.angVel_b_nb

    def calculate_linear_velocities(self): 
        '''
            Function to do most of the rotations to change from velocities from gazebo (map frame)
            into velocities in body & level body frame.
        '''

        ### Rotate velocity vectors ###
        # From map to local NED frame
        vel_n_mb = self.rotMat_mn @ self.vel_m_mb
        
        # From local NED to body frame
        self.vel_b_nb = self.droneGeom.rotFun_nb(self.tHeta_nb) @ vel_n_mb

        # From local body artificially level body frame
        rotMat_nl = self.droneGeom.rotFun_bl(self.tHeta_nb)
        self.vel_l_nb = rotMat_nl @ self.vel_b_nb

    # Helper function
    def __innovationYaw(self, yawMeasure, yawPredict):
        '''
        Function to return "geodesic" innovation on 0 to 2pi maping

        Takes two scalars as input and returns np shape (1,1)
        '''

        # Predefines error
        e = np.zeros((1,1), dtype = np.float32)

        # wraps measurement
        yawMeasure = np.remainder(yawMeasure, 2*np.pi)

        # Computes the two possible solutions
        e1 = yawMeasure - yawPredict

        if e1 < 0:
            e2 = 2*np.pi + e1
        else:
            e2 = e1 - 2*np.pi

        # Finds the shortest path
        if np.abs(e1) < np.abs(e2):
            e[0][0] = e1
        else:
            e[0][0] = e2

        return e


        

def main(args=None):
    rclpy.init(args=args)

    ground_truth_publisher = GroundTruth_Pub()

    rclpy.spin(ground_truth_publisher)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    ground_truth_publisher.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()