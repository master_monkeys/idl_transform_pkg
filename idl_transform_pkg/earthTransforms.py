


import numpy as np


'''
Notation

x_a_bc is a vactor resolved in a describing some measure from b to c

tHeta_bc is a vector of euler angle describing the relation from frame b to frame c

c_bc is a DCM matrix desscribing the relation ship from frame b to frame c... i.e c(tHeta_bc) gives a dcm from frame b to c

cf_xy, the f denotes that it is a function
'''

# Constants as pr WGS-84
radiusEquator   = 6378137.0             # meter
radiusPole      = 6356752.0             # meter
omega_e_ei      = 7.292115*10**(-5.0)   # rad/s
eccentricity    = 0.0818


def rotX(arg):
    '''
    Rotates the frame

    input   arg is a scalar
    output  is a np mat with shape (3,3)
    '''

    # pre calculates cos and sine
    c = np.cos(arg)
    s = np.sin(arg)

    return np.array([   [ 1, 0, 0],
                        [ 0, c,-s],
                        [ 0, s, c]])

def rotY(arg):
    '''
    Rotates the frame

    input   arg is a scalar
    output  is a np mat with shape (3,3)
    '''

    # pre calculates cos and sine
    c = np.cos(arg)
    s = np.sin(arg)

    return np.array([   [ c, 0, s],
                        [ 0, 1, 0],
                        [-s, 0, c]])

def rotZ(arg):
    '''
    Rotates the frame

    input   arg is a scalar
    output  is a np mat with shape (3,3)
    '''

    # pre calculates cos and sine
    c = np.cos(arg)
    s = np.sin(arg)

    return np.array([   [ c,-s, 0],
                        [ s, c, 0],
                        [ 0, 0, 1]])

def cf_bn(arg):


    # Parses data
    phi     = arg[0, 0]
    theta   = arg[1, 0]
    psi     = arg[2, 0]

    return rotZ(psi) @ rotY(theta) @ rotX(phi)

def cf_nb(arg):

    return np.transpose(cf_bn(arg))

def cf_ne(arg):
    '''
    Arg is lat long coordinates

    so arg has np shape (2,1) on form [l, u] [long lat]
    '''

    # Parsing data
    l = arg[0, 0]
    u = arg[1, 0]

    return rotZ(l) @ rotY(-u-np.pi)

def llh2ecef(arg):
    '''
    Function to transform from long lat and hight to ecef

    Input with np shape (3,1) on form [long lat hight]
    '''

    # Parsing data
    l = arg[0, 0]
    u = arg[1, 0]
    h = arg[2, 0]

    # Helpers
    inter1 = (radiusEquator*np.cos(u))**2 + (radiusPole*np.sin(u))**2
    N = radiusEquator**2/np.sqrt(inter1)

    # Calculating
    xe = (N+h)*np.cos(u)*np.cos(l)
    ye = (N+h)*np.cos(u)*np.sin(l)
    ze = ((radiusPole/radiusEquator)**2*N+h)*np.sin(u)

    return np.array([[xe],[ye],[ze]], dtype = np.float32)

def ssa(ang):
    return np.mod(ang+np.pi,2*np.pi)-np.pi

def flat2llh(arg,orig):
    '''
    Input   agr     np shape (3,1) [x, y, z]
            orig    np shape (3,1) [long_0, lat_0, h_ref]
    '''

    #Parsing data
    x = arg[0, 0]
    y = arg[1, 0]
    z = arg[2, 0]

    lRef    = orig[0, 0]
    muRef   = orig[1, 0]
    hRef    = orig[2, 0]

    # Computes
    inter1 = 1-(eccentricity*np.sin(muRef))**2
    Rn = radiusEquator/np.sqrt(inter1)
    Rm = Rn*(1-eccentricity**2)/np.sqrt(inter1)

    deltaL  = y*np.arctan2(1,Rm*np.sin(muRef))
    deltaMu = x*np.arctan2(1,Rn)
    
    l   = ssa(lRef + deltaL)
    mu  = ssa(muRef + deltaMu)
    h = hRef - z

    return np.array([[l],[mu],[h]], dtype = np.float32)

def rateTransform(arg):

    '''
    Input is a np mat of shape (3,1)

    Output is a np mat of shape (3,3)

    Transforms body rates to global rates
    '''
    
    # Parses data
    phi     = arg[0][0]
    theta   = arg[1][0]

    # Pre calculating cos, sin and tan
    cx = np.cos(phi)
    cy = np.cos(theta)
    sx = np.sin(phi)
    ty = np.tan(theta)

    # Defines transform
    w = np.array([  [1, sx*ty,  cx*ty],
                    [0,    cx,    -sx],
                    [0, sx/cy,  cx/cy]])

    return w


# test area

orig = np.array([[0.0],[0.0],[0.0]], dtype = np.float32)
orig = orig*np.pi/180

ned = np.array([[100000.0],[0.0],[0.0]], dtype = np.float32)

print(180/np.pi*flat2llh(ned, orig))