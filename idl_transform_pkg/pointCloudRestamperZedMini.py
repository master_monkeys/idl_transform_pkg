

import rclpy
from rclpy.node import Node
from rclpy.qos import qos_profile_sensor_data
from rclpy.time import Time
from sensor_msgs.msg import PointCloud2



class PointCloudReStamper(Node):

    def __init__(self):
        super().__init__('pc_re_stamper')

        # Subscribing to pc msg
        self.subscription_point_cloud_msg = self.create_subscription(   PointCloud2, 
                                                                        '/zedm/zed_node/point_cloud/cloud_registered', 
                                                                        self.pointCloud_callback, 
                                                                        1)
        self.subscription_point_cloud_msg # prevent unused variable warning

        self.publish_point_cloud_msg = self.create_publisher(       PointCloud2,
                                                                    'zed_mini_depth/points_unix_time',
                                                                    1)

        print('pc2_re_stamper created!')


    def pointCloud_callback(self, msg):

        # Restamps msg
        msgNew = PointCloud2()
        msgNew = msg
        msgNew.header.stamp = self.get_clock().now().to_msg()
        msgNew.header.frame_id = 'camera'

        # publishing msg
        self.publish_point_cloud_msg.publish(msgNew)


def main(args=None):
    rclpy.init(args=args)

    reStamper = PointCloudReStamper()

    rclpy.spin(reStamper)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    reStamper.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()



